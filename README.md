# Kim Hokanson - Portfolio

I'm a driven and versatile technical instructional designer with 7 years of experience in content design and strategy for self-service enablement and certification programs. My strengths are my agility, attention to detail, cross-functional collaboration, and commitment to user-focused design.

## Work samples

- [Videos](/work_samples/Videos/)
- [Courses](/work_samples/courses.md)
- [Hands-on labs](/work_samples/hands-on_labs.md)
- [Documentation](https://klhokanson.gitlab.io/documentation-example/)
- [Interactive modules](/work_samples/interactive_modules.md)
- [Infographics](/work_samples/Infographics/)

## 💻 Highlighted technical experience

### 🦊 GitLab

_GitLab is an AI-powered DevSecOps platform that enables software development teams to plan, build, test, and deploy their applications._

- Led the launch of GitLab University, our public-facing learning portal, including designing the UI and information architecture as well as implementing integrations with Google Analytics and eCommerce.
- Led a cross-functional user focus group on technical training gaps, synthesizing comments and feedback into the FY25 curriculum roadmap.
- Created a six course, modular, technical learning path on CI/CD, published for customers, partners, and internal team members, receiving the customer feedback that it was "the best training from GitLab".
- Developed two custom, persona-based learning paths for onboarding developers and system administrators for a high-touch customer engagement. 
- Rapidly created two self-paced courses on GitLab Duo, our AI add-on, including hands-on labs in our demo environment. 
- Wrote a comprehensive style guide and process documentation to create standards for content authoring.


### 🌎 PLEXSYS Interface Products

_PLEXSYS is a modeling and simulation software company with a suite of products for creating digital training environments._

- Converted a 1,000+ page technical manual of our flagship simulation software product into eLearning and instructor-led training (ILT) materials following three user personas.
- Managed a multi-year training initiative at the direction of the CEO, following 2-week sprint cycles in Jira to keep the project in scope and on time.
- Created training templates for use across the department to standardize delivery and increase production efficiency.
- Partnered with the Technical Documentation team to create single-source authoring between our user manuals and instructional training materials using MadCap Flare.
- Managed complex design projects through the end-to-end content development lifecycle, often balancing competing priorities.
- Awarded 4 merit-based bonuses within the first year of employment.



## ✅ Skills

- Agile project management
- Cross-functional remote collaboration
- Content strategy
- Curriculum development
- Hands-on labs
- Technical writing & editing
- Video creation

### 🎧 Tools

- Adobe Creative Suite (InDesign, Illustrator, Premiere Pro)
- Articulate 360 (Storyline, Rise)
- Atlassian (Jira, Confluence)
- Audacity
- Canva
- GitLab
- Learning management systems (Thought Industries, Wordpress, Moodle)
- TechSmith (Camtasia, Snagit)
- VS Code

### 🖥️ Tech

- HTML, CSS - proficient
- Markdown - proficient
- Javascript - introductory
- Python - introductory

## 🎓 Education and certifications

- Ed.S., Educational Psychology
- B.S., Human Development

### 🏆 Technical Certifications

- Front End Web Development, _Team Treehouse_
- UX Design Professional, _Google_
- Certified DevOps Professional, _GitLab_
- Google Cloud Digital Leader, _Google_
- Certified Agile Project Management, _GitLab_
- Certified CI/CD Associate, _GitLab_
- Certified Security Specialist, _GitLab_
