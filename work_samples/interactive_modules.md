# Interactive eLearning modules

Using Articulate Storyline, I've originally authored and developed highly interactive eLearning modules for self-paced training. Two examples can be found below:

### [▶️ Microsoft Teams simulated exercise](https://klhokanson.gitlab.io/teams-interactive-module/)

This exercise helps learners reinforce the concepts they learned throughout the module as part of an employee onboarding training for Microsoft Teams. Created in Storyline, this interaction lets learners practice in a simulated environment.

### [🎯 Objective builder](https://klhokanson.gitlab.io/objective-builder/)

As part of a modularized introduction to training course, this guided practice helps learners create a measurable learning objective following Bloom's taxonomy.