# Self-paced courses

My approach to developing self-paced content leans heavily on breaking content into bite-sized modules and chaining concepts together, mixed with interactive practice. This modularized format is both an effective instructional strategy as well as a way to make more agile content updates for rapidly developing software products. 

## [GitLab Duo Enterprise Learning Path](https://university.gitlab.com/learning-paths/gitlab-duo-enterprise-learning-path)

This two course learning path is my most recent content development project, created as part of a CEO initiative related to AI at GitLab. With these courses, I initiated a new content development process of authoring all written content directly within GitLab, using a merge request to collaboration with product managers and subject matter experts across GitLab. The learning path results in a certificate of completion, shareable on LinkedIn. 

## [GitLab CI Fundamentals Learning Path](https://university.gitlab.com/learning-paths/gitlab-ci-fundamentals)

### Background

The original GitLab CI/CD course was a monolithic 8-hour certification course that was several versions behind the most recent release of GitLab. I already had ideas for needed fixes to the course to better align with adult learning theory best practices, but I also wanted to get a better sense of the feedback from others on how it could be improved. I gathered data from:

- End of course surveys
- LMS course participation statistics (e.g., registrations, completions, quiz scores)
- Internal team members including:
- Customer success managers​
- Technical writers
- Product managers

Some of this feedback included comments like:

- The course has too many iFrames that just point to documentation pages without any context
- There aren't enough quiz questions
- Course content was too long (average completion rate was 28%)
- Several hands-on lab had confusing instructions
- Content was out of date

### Results

After the six-course design project was complete (process outlined more below), some of the results include:

- Increased average course completion percentage by 115%
- Received end-of-course feedback from a customer that it was "the best training I found from GitLab"

### Project Plan

I created a detailed project plan with a proposal to completely restructure the content into a 6-course learning path. I made an epic in GitLab to track my work and individual issues for each course, with a release cadence of publishing one course every three weeks. This drastically improved the content publishing cycles that were occurring quarterly.

### My Role 

I led this project end-to-end through the course design lifecycle. After identifying the project needs, subject matter experts, and work parameters, I began writing new instructional content. I started with an outline of the learning path objectives, broken up into courses, then drilled down into the more detailed scripts. I created and edited course videos, designed graphics, and built the courses in the learning management system. When the project was complete, I initiated marketing efforts and pulled reports on new course feedback and statistics.

### Improvements

- Added 26 instructional videos to add content variety, incorporate hands-on examples, and add instructor presence
- Created a question bank of 50+ new knowledge check questions to reinforce learning concepts
- Revised learning objectives to target higher order skills
- Updated for accessibility including closed captions, alt text, plain text versions of instructional images, appropriate heading styles, and audio narration for text-only pages
- Added new course images including infographics and gifs to improve visual interest
- Revised step-by-step instructions for hands-on labs for clarity and increased realism

### Access the content

The learning path is publicly available on GitLab University - see the [registration page](https://university.gitlab.com/learning-paths/gitlab-ci-fundamentals). 